'use client'

import Link from "next/link";
import {useRef} from "react";


export default function Header({currentPage}) {

  const scrollRef = useRef(null);

  if (typeof window !== "undefined") {
    let lastScroll = 0
    window.addEventListener('scroll', () => {
      var currentScroll = window.pageYOffset
  
      if(currentScroll == 0) {
        // ifZero()
        scrollRef.current.classList.remove("scroll-down")
        scrollRef.current.classList.remove("scroll-up")
      }
  
      if (currentScroll > lastScroll && !scrollRef.current.classList.contains('scroll-down')) {
        // ifDown()
        scrollRef.current.classList.remove("scroll-up")
        scrollRef.current.classList.add("scroll-down")
      }
  
      if (currentScroll < lastScroll && scrollRef.current.classList.contains('scroll-down')) {
        // ifUp()
        scrollRef.current.classList.remove("scroll-down")
        scrollRef.current.classList.add("scroll-up")
      }
  
      lastScroll = currentScroll
    })
  }


    return (
      <header className="z-40">
        <div ref={scrollRef} className="z-40 navbar bg-suit-orange fixed">
          <div className="navbar-start mx-20">
            <div className="dropdown">
              <div tabIndex={0} role="button" className="btn btn-ghost lg:hidden">
                <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M4 6h16M4 12h8m-8 6h16" /></svg>
              </div>
              <ul tabIndex={0} className="menu menu-sm dropdown-content mt-3 z-[1] p-2 shadow bg-suit-white rounded-box w-52">
              <li><Link href="/work" className={currentPage == "work" ? "underline underline-offset-8" : ""}>Work</Link></li>
              <li><Link href="/about" className={currentPage == "about" ? "underline underline-offset-8" : ""}>About</Link></li>
              <li><Link href="/services" className={currentPage == "services" ? "underline underline-offset-8" : ""}>Services</Link></li>
              <li><Link href="/" className={currentPage == "ideas" ? "underline underline-offset-8" : ""}>Ideas</Link></li>
              <li><Link href="/careers" className={currentPage == "careers" ? "underline underline-offset-8" : ""}>Careers</Link></li>
              <li><Link href="/contact" className={currentPage == "contact" ? "underline underline-offset-8" : ""}>Contact</Link></li>
              </ul>
            </div>
            <div>
              <img src="/suitmedia-logo.png" alt="" className="h-14" />
            </div>
          </div>
          <div className="navbar-end hidden lg:flex mr-5 w-full">
            <ul className="menu menu-horizontal px-1 text-suit-white font-semibold">
              <li><Link href="/work" className={currentPage == "work" ? "underline underline-offset-8" : ""}>Work</Link></li>
              <li><Link href="/about" className={currentPage == "about" ? "underline underline-offset-8" : ""}>About</Link></li>
              <li><Link href="/services" className={currentPage == "services" ? "underline underline-offset-8" : ""}>Services</Link></li>
              <li><Link href="/" className={currentPage == "ideas" ? "underline underline-offset-8" : ""}>Ideas</Link></li>
              <li><Link href="/careers" className={currentPage == "careers" ? "underline underline-offset-8" : ""}>Careers</Link></li>
              <li><Link href="/contact" className={currentPage == "contact" ? "underline underline-offset-8" : ""}>Contact</Link></li>
            </ul>
          </div>
          {/* <div className="navbar-end">
            <a className="btn">Button</a>
          </div> */}
        </div>
      </header>
    );
  }