import Header from "../Header";

export default function Contact() {
    return (
        <main>
            <Header currentPage={"contact"}></Header>
            <div className="h-16"></div>
            <div className="w-full flex justify-center mt-20">
                <p className="text-3xl font-bold">This page is still under construction</p>
            </div>
        </main>
    );
}