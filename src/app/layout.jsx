import { Inter } from "next/font/google";
import "./globals.css";

const inter = Inter({ subsets: ["latin"] });

export const metadata = {
  title: "Suitmedia",
  description: "Suitemedia Frontend Intern Submission",
};

export default function RootLayout({ children }) {
  return (
    <html lang="en" data-theme="light">
      <body className="bg-suit-white overflow-y-scroll no-scrollbar">{children}</body>
    </html>
  );
}
