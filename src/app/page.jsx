'use client'

import Header from "./Header";
import { useEffect, useState } from "react";


export default function Home() {


  // memakai fungsi karena api suitmedia sedang tidak bisa diakses
  function generateDummyApiResponse(pageNumber, pageSize, sort) {
    Date.prototype.addDays = function(days) {
      var date = new Date(this.valueOf());
      date.setDate(date.getDate() + days);
      return date;
    }

    var date = new Date();


    var arr = []
    for(let i = 1; i<=100; i++) {
      var data = {
        "id": i,
        "tanggal": date.addDays((-100+i)).getDate().toString() + "/" + date.addDays((-100+i)).getMonth().toString() + "/" + 2024,
        "judul" : "Ini adalah artikel dummy, tujuan dari berita ini adalah mengisi projek ini",
        "imgUrl" : "https://media.rnztools.nz/rnz/image/upload/s--ddpMQ0P9--/ar_16:10,c_fill,f_auto,g_auto,q_auto,w_1050/v1624925138/4Q3W9G0_knowledge_3_4795"
      }
      arr.push(data)
    }
    var return_value = arr.slice((pageNumber*pageSize)-(pageSize-1)-1, (pageNumber*pageSize))
    if(sort == "published_at") {
      return_value.sort((a, b) => (a.id < b.id ? 1 : -1));
    } else if(sort == "-published_at") {
      return_value.sort((a, b) => (a.id > b.id ? 1 : -1));
    }

    return return_value
  }



  const [showPage, setShowPage] = useState(10)
  const [page, setPage] = useState(1)
  const [sort, setSort] = useState("published_at")
  const [articles, setArticles] = useState([])

  useEffect(() => {
    setArticles(generateDummyApiResponse(page, showPage, "published_at"))
  }, [])

  function updateNews(pageNumber, pageSize, sort) {
    setArticles(generateDummyApiResponse(pageNumber, pageSize, sort))
  }

  return (
    <main>
      <Header currentPage="ideas"></Header>
      <div className="h-16"></div>
      <div className="h-96 w-full bg-fixed bg-parallax bg-cover grid-cols-1 justify-items-center relative z-0">
        <div className="pt-32 w-full text-center">
        <p className="text-suit-white text-5xl pb-2">Ideas</p>
        <p className="text-suit-white text-xl">Where all our great things began</p>
        
        </div>
        <div className="up-arrow absolute h-24"></div>

      </div>
      <div className="my-20 mx-20 flex justify-between">
        <div>
          <p>Showing {(page*showPage)-(showPage-1)} - {page*showPage} out of 100</p>
        </div>
        <div className="flex w-1/2">
          <p className="pt-3 mr-3 text-sm whitespace-nowrap">Showing per page :</p>
          <select className="select select-bordered w-full max-w-xs" onChange={e => {
            setShowPage(e.target.value)
            setPage(1)
            updateNews(1, e.target.value, sort)
            }}>
            <option value={10}>10</option>
            <option value={20}>20</option>
            <option value={50}>50</option>
          </select>

          <p className="pt-3 ml-9 mr-3 text-sm whitespace-nowrap">Sort by :</p>
          <select className="select select-bordered w-full max-w-xs" onChange={e => {
            setSort(e.target.value)
            updateNews(page, showPage, e.target.value)
            }}>
            <option value={'published_at'}>Newest</option>
            <option value={'-published_at'}>Oldest</option>
          </select>
        </div>
      </div>

      
      <div className="grid grid-cols-4 gap-4 mx-20 mb-20">
        {articles.map((data, i) =>
          <div className="card w-full bg-base-100 shadow-xl" key={i}>
            <figure className="h-30"><img src={data.imgUrl} alt="idea image" /></figure>
              <div className="card-body">
                  <p>{data.tanggal}</p>
                  <h3 className="card-title line-clamp-3">{data.judul}</h3>
                  
              </div>
          </div>
        )}

      </div>

      <div className="join flex justify-center mb-5">
      <button className="join-item btn btn-md" onClick={() => {
        if(page != 1) {
          updateNews(page-1, showPage, sort)
          setPage(page-1)
        }
      }}>«</button>
      {[...Array(100/showPage)].map((elementInArray, index) => ( 
    <button className={"join-item btn btn-md" + (page == index+1 ? " btn-active" : "")} key={index} onClick={() => {
      updateNews(index+1, showPage, sort)
      setPage(index+1)
    }}> {index+1} </button> 
    ) 
)}
  <button className="join-item btn btn-md" onClick={() => {
        if(page != 100/showPage) {
          updateNews(page+1, showPage, sort)
          setPage(page+1)
        }
      }}>»</button>
</div>
    </main>
  );
}
